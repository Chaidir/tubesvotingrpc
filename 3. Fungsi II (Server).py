def count():
        lock.acquire()
        system('clear')
        total = 0
        for i in kandidat:
            total = total + kandidat[i]
        msg = "Hasil Perhitungan: \n"
        if total != 0:
            for i in kandidat:
                hasil_vote = (kandidat[i] / total) * 100
                msg = msg + i + ":" + str(hasil_vote) + "%\n"
            print(msg)
        lock.release()
        return msg
    server.register_function(count)
    
    def daftarpemilihtetapCheck(dpt):
        return daftarpemilihtetap.get(dpt) != None
    server.register_function(daftarpemilihtetapCheck)

    def ktpCheck(id,dpt):
        tmp = daftarpemilihtetap[dpt].get(id)
        if tmp != None:
            return tmp
        else:
            return "Bukan Nomor KTP Anda "+dpt
    server.register_function(ktpCheck)
    
    def getdaftarpemilihtetap():
        msg = ""
        if nama_pemilih != None:
            return str(nama_pemilih)
        else:
            return "pemilih Tidak Terdaftar"
    server.register_function(getdaftarpemilihtetap)
