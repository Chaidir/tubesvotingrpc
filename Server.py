import threading
import time
from os import system
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

with SimpleXMLRPCServer(("127.0.0.1", 8000),requestHandler=RequestHandler) as server:
    server.register_introspection_functions()

    kandidat = {"Pudipai": 0, "Tseris": 0}
    nama_kandidat = {"Pudipai", "Tseris"}
    daftarpemilihtetap = {
        "Abi Siregar": {"011123456789": False,},
        "Andi Mandala": {"012123456789": False,},
        "Aji Waluyo": {"013123456789": False},
        "Aji Oaoe": {"014123456789": False},
        "Bayu Sapta Aji": {"015123456789": False},
        "Bobby Abdul": {"016123456789": False},
        "Bono Adventure": {"017123456789": False},
        "Chikal Adli": {"018123456789": False},
        "Tony Hawk": {"021123456789": False},
        "Tony Bean": {"022123456789": False},
        "Uus Hafied": {"023123456789": False},
        "Violeta Puspita": {"024123456789": False},
        "Winkie Setyo": {"025123456789": False}
    }
    nama_pemilih= {"Abi Siregar",
                   "Andi Mandala",
                   "Aji Waluyo",
                   "Aji Oaoe",
                   "Bayu Sapta Aji",
                   "Bobby Abdul",
                   "Bono Adventure",
                   "Chikal Adli",
                   "Tony Hawk",
                   "Tony Bean",
                   "Uus Hafied",
                   "Violeta Puspita",
                   "Winkie Setyo"
                   
                   }
  
    lock = threading.Lock()
    
    def vote(x, id, dpt):
        msg = ""
        lock.acquire()
        if kandidat.get(x) != None:
            kandidat[x] = kandidat[x] + 1
            msg = "Anda berhasil memilih kandidat anda"
            daftarpemilihtetap[dpt][id] = True
            system('clear')
        else:
            msg = "Tidak Ada Kandidat"
        lock.release()
        return msg
    server.register_function(vote)

    def count():
        lock.acquire()
        system('clear')
        total = 0
        for i in kandidat:
            total = total + kandidat[i]
        msg = "Hasil Perhitungan: \n"
        if total != 0:
            for i in kandidat:
                hasil_vote = (kandidat[i] / total) * 100
                msg = msg + i + ":" + str(hasil_vote) + "%\n"
            print(msg)
        lock.release()
        return msg
    server.register_function(count)
    
    def daftarpemilihtetapCheck(dpt):
        return daftarpemilihtetap.get(dpt) != None
    server.register_function(daftarpemilihtetapCheck)

    def ktpCheck(id,dpt):
        tmp = daftarpemilihtetap[dpt].get(id)
        if tmp != None:
            return tmp
        else:
            return "Bukan Nomor KTP Anda "+dpt
    server.register_function(ktpCheck)
    
    def getdaftarpemilihtetap():
        msg = ""
        if nama_pemilih != None:
            return str(nama_pemilih)
        else:
            return "pemilih Tidak Terdaftar"
    server.register_function(getdaftarpemilihtetap)

    def getKandidat():
        if kandidat != None:
            return str(nama_kandidat)
        else:
            return "Tidak ada Kandidat yang terdafar"
    server.register_function(getKandidat)

    

    print("Server berjalan...")
    server.serve_forever()
