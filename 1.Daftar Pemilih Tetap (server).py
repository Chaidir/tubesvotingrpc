import threading
import time
from os import system
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

with SimpleXMLRPCServer(("127.0.0.1", 8000),requestHandler=RequestHandler) as server:
    server.register_introspection_functions()

    kandidat = {"Pudipai": 0, "Tseris": 0}
    nama_kandidat = {"Pudipai", "Tseris"}
    daftarpemilihtetap = {
        "Abi Siregar": {"011123456789": False,},
        "Andi Mandala": {"012123456789": False,},
        "Aji Waluyo": {"013123456789": False},
        "Aji Oaoe": {"014123456789": False},
        "Bayu Sapta Aji": {"015123456789": False},
        "Bobby Abdul": {"016123456789": False},
        "Bono Adventure": {"017123456789": False},
        "Chikal Adli": {"018123456789": False},
        "Tony Hawk": {"021123456789": False},
        "Tony Bean": {"022123456789": False},
        "Uus Hafied": {"023123456789": False},
        "Violeta Puspita": {"024123456789": False},
        "Winkie Setyo": {"025123456789": False}
    }
    nama_pemilih= {"Abi Siregar",
                   "Andi Mandala",
                   "Aji Waluyo",
                   "Aji Oaoe",
                   "Bayu Sapta Aji",
                   "Bobby Abdul",
                   "Bono Adventure",
                   "Chikal Adli",
                   "Tony Hawk",
                   "Tony Bean",
                   "Uus Hafied",
                   "Violeta Puspita",
                   "Winkie Setyo"
                   
                   }
  
