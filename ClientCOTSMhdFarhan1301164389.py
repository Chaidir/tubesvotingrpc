import xmlrpc.client
from os import system

client = xmlrpc.client.ServerProxy("http://localhost:8000")

def start():
    output = ""
    system('clear')
    while True:
        print('Menu : \n'
              '1. Vote \n'
              '2. Kenapa Suara Saya Tidak Sah?\n')
        pilihan = input("Pilih menu : ")
        if pilihan == "1":
            output = ""
            while pilihan == "1":
                system('clear')
                print('Daftar Pemilih Tetap : '+client.getdaftarpemilihtetap())
                daftarpemilihtetap = input("Masukan Nama Anda  : ")
                if daftarpemilihtetap == "":
                    pilihan = None
                    output = ""
                    system('clear')
                    
                elif client.daftarpemilihtetapCheck(daftarpemilihtetap):
                    system('clear')
                    print("Nama Anda : " + daftarpemilihtetap)
                    id = input("Masukan Nomor KTP Anda : ")
                    check = client.ktpCheck(id, daftarpemilihtetap)
                    if check == False:
                        system('clear')
                        print("Nama Anda : " + daftarpemilihtetap)
                        print("NO KTP      : " + id)
                        print('Kandidat     : ' + client.getKandidat())
                        i = input("Silahkan Pilih Kandidat Anda : ")
                        client.vote(i, id , daftarpemilihtetap)
                        client.ceksuara(i, id, daftarpemilihtetap)
                        client.count()
                        

                        
                    elif check == True:
                        output = "Anda sudah memilih"
                        
                    else:
                        output = check
                else:
                    output = "Pemilih "+daftarpemilihtetap+" Tidak Terdaftar"

        #Penambahan fitur untuk penjelasan suara yang tidak sah oleh Mhd. Farhan (1301164389)
        elif pilihan == "2":
            print("Pastikan ketika anda memilih kandidat harus sesuai dengan nama yang tertera, termasuk huruf besar dan kecil.")
            back = input("'Enter' Untuk Kembali.")
            print()
            system('clear')
start()
