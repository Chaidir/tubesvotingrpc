import xmlrpc.client
from os import system
from datetime import datetime

client = xmlrpc.client.ServerProxy("http://localhost:8000")

def start():
    output = ""
    system('clear')
    # Ambil batas waktu
    jam_selesai = datetime.now().replace(hour = client.jam_selese(), minute=0)
    while True:
        print('Menu : \n'
              '1. Vote \n')
        pilihan = input("Pilih menu : ")
        if pilihan == "1":
            output = ""
            while pilihan == "1":
                system('clear')
                #penambahan batasan waktu
                sisa = (jam_selesai - datetime.now()).seconds
                jam, remainder = divmod (sisa, 3600)
                menit, second = divmod (remainder, 60)
                print("Waktu tersisa ", jam, ":", menit)
                print('Daftar Pemilih Tetap : '+client.getdaftarpemilihtetap())
                daftarpemilihtetap = input("Masukan Nama Anda  : ")
                if daftarpemilihtetap == "":
                    pilihan = None
                    output = ""
                    system('clear')
                    
                elif client.daftarpemilihtetapCheck(daftarpemilihtetap):
                    system('clear')
                    print("Nama Anda : " + daftarpemilihtetap)
                    id = input("Masukan Nomor KTP Anda : ")
                    check = client.ktpCheck(id, daftarpemilihtetap)
                    if check == False:
                        system('clear')
                        print("Nama Anda : " + daftarpemilihtetap)
                        print("NO KTP      : " + id)
                        print('Kandidat     : ' + client.getKandidat())
                        i = input("Silahkan Pilih Kandidat Anda : ")
                        output = client.vote(i, id , daftarpemilihtetap)
                        client.count()
                        
                    elif check == True:
                        output = "Anda sudah memilih"
                    else:
                        output = check
                else:
                    output = "Pemilih "+daftarpemilihtetap+" Tidak Terdaftar"


start()
