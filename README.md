**[Tugas-Besar_Voting_RPC]**

Nama:

•	Hatta Chaidir

•	Naufal Afif

•	Muhamad Nurihsan

•	Muhammad Farhan Intan I

Kelas: IF-40-12

**Requirement**
•	Python 3.6


**Penambahan Fitur**

•	Hatta Chaidir (VisiMisi & Total Pemilih)

•	Naufal Afif (Memberi batasan waktu memilih pasangan calon)

•	Muhamad Nurihsan (Hapus Pemilih)

•	Muhammad Farhan Intan I (Perhitungan suara tidak sah & Penjelasan kenapa suara tidak sah)

